﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnorCollision : MonoBehaviour {

    public Collider[] cols;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision hit)
    {
        if (hit.gameObject.tag == "Player")
        {
            cols[0].GetComponent<Collider>().isTrigger = true;
            cols[1].GetComponent<CapsuleCollider>().isTrigger = true;
        }
    }
    void OnCollisionExitr(Collision hit)
    {
         cols[0].GetComponent<Collider>().isTrigger = false;
         cols[1].GetComponent<CapsuleCollider>().isTrigger = false;
    }
}
