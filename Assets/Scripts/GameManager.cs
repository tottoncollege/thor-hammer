﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public Text ScoreText; 
	public Text PromptText;
	public GameObject PromptObj;
	public HammerThrow hammerScript;
	public bool CanPromt = false;
	public bool UsedPromt = false;
    public int DoorNum = 0;
    public GameObject[] doors = new GameObject[2];
    AreaTrigger Thetrigger;
	int PromptOption = 0;
    List<GameObject> Hammers = new List<GameObject>();
	int Score = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Update what should be displayed in the promt box every frame
		ScoreText.text = Score.ToString ();

		if (CanPromt == true) 
		{
			if (Input.GetKeyDown (KeyCode.E)) 
			{
				switch (PromptOption)
				{
				case 0:
					    PickUpHammer ();
					    ShowText ("Left click to throw Thors hammer. \n Hold longer to throw further.");
                        Thetrigger.Used = true;
					break;
                case 1:
                        doors[DoorNum].GetComponent<Animation>().Play();
                        Thetrigger.Used = true;
                        HideText();
                    break;

				}

			}
			if (Input.GetMouseButtonUp(0)) 
			{
				HideText ();
				CanPromt = false;
				UsedPromt = true;

			}

		}
	}

	public void AddToScore (int Addition, GameObject HammerHit)
	{
        //Check if this hammer has been used to give points before.
        if (!Hammers.Contains(HammerHit))
        {
            //If not, add to the list that have given points
            Hammers.Add(HammerHit);
            //And add the points.
            Score += Addition;
            Debug.Log(Addition + " added to score.");
        }
		
	}

	public void Prompt (bool Bool, int optionNum, AreaTrigger trigger)
	{
        Thetrigger = trigger;
		CanPromt = Bool;
		PromptOption = optionNum;
        switch (optionNum)
        {
            case 0:
                ShowText("Press 'E' to pick-up Thors hammer.");
                break;
            case 1:
                ShowText("Press 'E' to open door.");
                break;
        }
	}

	public void ShowText (string text)
	{
		PromptObj.SetActive (true);
		PromptText.text = text;
	}

	public void HideText ()
	{
		PromptObj.SetActive (false);
		PromptText.text = "";
	}

	public void PickUpHammer ()
	{
		hammerScript.CanThrow = true;
	}


}
