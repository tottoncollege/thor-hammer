﻿using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ReverseNormalsEditorMode))]
public class CustomEditorReverse : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ReverseNormalsEditorMode myScript = (ReverseNormalsEditorMode)target;
        if (GUILayout.Button("Reverse"))
        {
            myScript.Reverse();
        }
    }
}
