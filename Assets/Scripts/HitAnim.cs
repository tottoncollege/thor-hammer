﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitAnim : MonoBehaviour {

    bool down = false;
    public int PointsToGive = 5;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        //Check if collision was with hammer, and check if this object has already been hit down. 
        if (collision.gameObject.tag == "Hammer" && down == false)
        {
            //set hit down to true and play the animation
            down = true;
            this.gameObject.GetComponent<Animation>().Play();
            //Tell game manager to add points.
            GameObject.Find("GameManager").GetComponent<GameManager>().AddToScore(PointsToGive, collision.gameObject);
        }
    }
}
