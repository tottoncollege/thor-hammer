﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HammerThrow : MonoBehaviour {

    public GameObject ThorsHammer;
    public GameObject Ethan;
    GameObject tmp;
	public Image CrossHairFill;
	public bool CanThrow = false; 
    bool timer = false;
    float theTimer = 10.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Has the player picked up the hammer so they can use it?	
		if (CanThrow == true)
        {
            //Check if the player has pressed the left click down on the mouse
			if (Input.GetMouseButtonDown (0))
            {
                //Allow the timer to start
				theTimer = 10.0f;
				timer = true;
			}
            //Check if the player has let go of the left click on the mouse
            if (Input.GetMouseButtonUp (0))
            {
                //Stop the timer and throw a hammer.
				timer = false;
				tmp = Instantiate (ThorsHammer, new Vector3 (this.gameObject.transform.position.x, this.transform.position.y, this.transform.position.z), this.gameObject.transform.rotation) as GameObject;
				tmp.transform.rotation = this.gameObject.GetComponentInParent<Transform> ().rotation;
				tmp.GetComponent<Rigidbody> ().AddRelativeForce (0.0f, theTimer * 0.2f , theTimer);
				theTimer = 10.0f;
			}
		}

        //If the timer is active, count. 
		if (timer == true)
        {
            //Stop counter when it reaches 500.
			if (theTimer <= 500.0f)
				theTimer += Time.deltaTime * 200.0f;
            //Update the crosshair fill amount to a percentage, converting 0/500 to 0/1.
			CrossHairFill.fillAmount = 1 / (500.0f / theTimer);
		} 
		else //Reset the fill amount
			CrossHairFill.fillAmount = 0;





        


	}
}
