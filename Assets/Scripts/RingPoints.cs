﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingPoints : MonoBehaviour {

    public int PointsToGive = 1;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        //Was the objec that hit a hammer?
        if (collision.gameObject.tag == "Hammer")
        {
            //If so, tell the game manager about it, and how many points it is worth.
            GameObject.Find("GameManager").GetComponent<GameManager>().AddToScore(PointsToGive, collision.gameObject);
        }
        
    }
}
