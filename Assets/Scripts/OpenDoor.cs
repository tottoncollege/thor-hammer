﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour {

    bool Open = false;


    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Hit");
        if (Open == false && collision.gameObject.tag == "Player")
        {
            this.gameObject.GetComponent<Animation>().Play();
            Open = true;
        }
    }
}
