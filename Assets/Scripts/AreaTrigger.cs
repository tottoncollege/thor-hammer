﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTrigger : MonoBehaviour {

	public GameManager gameManager;
	public int OptionNum = 0;
    public bool IsDoor = false;
    public int DoorNum = 0;
    public bool Used = false;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    /*
    //This function will be called when another gameobject with a colider intersects this gameobjects collider. 
	void OnTriggerEnter(Collider collider)
	{
        //Check what collided with this gameObject. Was it the player? Has this area trigger already been used?
        if (collider.gameObject.tag == "Player" && Used == false) 
		{
            //If it is the player and this trigger has not been used, contact the game manager to display a prompt. 
            gameManager.Prompt(true, OptionNum, this);
            //Is this area trigger for a door?
            if (IsDoor)
            {
                //If so, tell the game manager that the player is trying to open a door.
                gameManager.DoorNum = DoorNum;
            }
		}
	}



    //This function will be called when another gameobject with a colider exits this gameobjects collider. 
    void OnTriggerExit(Collider collider)
	{
        //should the promt be deactivted? 
		if (Used == false && gameManager.CanPromt == true) 
		{
            //If so, set it to false and hide any text curently being displayed. 
			gameManager.Prompt (false, OptionNum, this);
			gameManager.HideText ();
		}
	}
    */
    
}
