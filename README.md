# Welcome to Totton College #


### Your Totton College Game Development taster starts here! ###
## Task 1 - 3D Modelling! ##
+ Please visit this link and sign up for a free student account, stating Totton College/Nacro as your accademic institution:
* https://www.autodesk.com/education/free-software/3ds-max
- Once you have an account, continue with the download and installation of the software.
+ Please open the following link to get started with the tutorial
* https://docs.google.com/document/d/198ygCuXs91Vk4kAlu5M6UbetlmsqqsVWYMSZlW8aFCE/edit?usp=sharing


## Task 2 - Unity! ##
+ Please open these Google Documents and follow the instructions. 
* https://docs.google.com/document/d/1VtFh5F3MvSfv2lbW-eOokRqGoaGgiwn5lDlxkHbwFhQ/edit?usp=sharing
* https://docs.google.com/document/d/1xSEAByWMU9zxuCRySWT4AFIK6M0E_7FsYcE7lxQ4T0c/edit?usp=sharing
- If completing this offline, please click 'Downloads' on the left and then 'Download Reposotpry'. You will find PDF versions of the Documents from the above links within the zip, along with the project files. 

### What is this repository for? ###

This Repo is for upcoming Totton College  students wishing to study our Game Development course. This is a second half of a tutorial following on from a 3D modeling introduction. The model you make is used in this project. This tutorial will walk you through, step by step, how to use some of the Unity Game Engine features. 

### How do I get set up? ###

Download this repository and follow the instructions in the Google Drive or PDF documents.

